#!/bin/bash

if [ -z ${PROJECT_ROOT+x} ]; then
    echo "please source \"envsetup.sh\""
    exit 1
fi

if [ $# -ne 2 ]; then
    echo "USAGE: $lvs <block name> <cell name>"
    exit 1
fi

echo "Block name : \""$1"\""
echo "Cell name  : \""$2"\""

MAG_FILE=$PROJECT_ROOT/ip/$1/magic/$2.mag
SCH_FILE=$PROJECT_ROOT/ip/$1/xschem/$2.sch
TMP_PATH=$PROJECT_ROOT/tmp/lvs

echo "Schematic  : \""$SCH_FILE"\""
echo "Layout     : \""$MAG_FILE"\""
echo "Output     : \""$TMP_PATH"\""

mkdir -p $TMP_PATH

RUN_DIR=$PWD
cd $TMP_PATH
# clean the LVS folder
rm -rf ../../tmp/lvs/*


# create magic tcl script
echo "load $MAG_FILE
extract all
ext2spice lvs
ext2spice -o $2_lvsmag.spice
exit" > extract_lvs.tcl

# invoke magic for lvs spice netlist extraction
magic -rcfile $PDK_ROOT/sky130A/libs.tech/magic/sky130A.magicrc -noconsole -dnull extract_lvs.tcl &

# wait for lvs netlist to be generated
printf "Waiting for LVS extracted netlist to be generated.."
while [ ! -s $2_lvsmag.spice ]
    do
    printf "."
    sleep 0.25
done
echo " "

# invoke xschem for spice netlist generation
xschem --rcfile=$PROJECT_ROOT/xschemrc -n -q -o "$TMP_PATH" --tcl "set top_subckt 0" "$SCH_FILE"
mv $2.spice $2_lvssch.spice

#sed -i '$s,.end,.include '"$PDK_ROOT"'\/skywater-pdk/libraries/sky130_fd_pr/latest/models/sky130.lib.spice\n.end,g' "$2_lvssch.spice"
netgen -batch lvs $2_lvsmag.spice $2_lvssch.spice $PDK_ROOT/sky130A/libs.tech/netgen/sky130A_setup.tcl lvs_report.log

echo "REPORT HERE: $TMP_PATH/lvs_report.log"
