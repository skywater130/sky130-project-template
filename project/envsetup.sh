#!/bin/bash

# update the below paths <<<<<<<<
export PDK_ROOT=$HOME/repos/sky130pdk
export ANALOGLIB_ROOT=$HOME/repos/analoglib
# >>>>>>>>


#export XSCHEM_ROOT=/usr/local/share/xschem

export PROJECT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

alias sch="xschem --rcfile $PROJECT_ROOT/xschemrc"
alias lay="$PROJECT_ROOT/layout.sh"
#magic -d OGL -rcfile $PDK_ROOT/sky130A/libs.tech/magic/sky130A.magicrc $1
alias lvs="$PROJECT_ROOT/lvs.sh"
alias pex="$PROJECT_ROOT/pex.sh"

echo "Analog Design Environment Setup Complete!"
