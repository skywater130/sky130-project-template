#!/bin/bash

if [ -z ${PROJECT_ROOT+x} ]; then
    echo "please source \"envsetup.sh\""
    exit 1
fi

if [ $# -ne 2 ]; then
    echo "USAGE: $pex <block name> <cell name>"
    exit 1
fi

echo "Block name : \""$1"\""
echo "Cell name  : \""$2"\""

MAG_FILE=$PROJECT_ROOT/ip/$1/magic/$2.mag
TMP_PATH=$PROJECT_ROOT/tmp/pex

echo "Layout     : \""$MAG_FILE"\""
echo "Output     : \""$TMP_PATH"\""

mkdir -p $TMP_PATH

RUN_DIR=$PWD
cd $TMP_PATH
# clean the PEX folder
rm -rf ../../tmp/pex/*


# create magic tcl script
# TODO - use 'extresist' in magic for parasitic resistance extraction when it works - doesn't work at the moment
echo "load $MAG_FILE
extract all
ext2spice lvs
ext2spice cthresh 0
ext2spice -o $2_pex.spice
exit" > extract_pex.tcl

# invoke magic for lvs spice netlist extraction
magic -rcfile $PDK_ROOT/sky130A/libs.tech/magic/sky130A.magicrc -noconsole -dnull extract_pex.tcl &
