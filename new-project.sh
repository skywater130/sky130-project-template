#!/bin/bash

if [ "$#" -ne 2 ]; then
	echo "USAGE: ./new-project.sh <dir to create project under> <name of project>"
	echo "E.g. executing \"./new-project.sh ~/caravel columbus\" will create \"~/caravel/columbus/\" along with all necessary subfolders/files"
	exit
fi

echo "Creating project" $2 "under" $1 "..."

TEMPLATE_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

mkdir -p $1/$2
cp -r $TEMPLATE_ROOT/project/* $2/$1/

ls -l $1/$2/
echo "Done!"
